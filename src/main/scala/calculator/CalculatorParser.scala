package calculator

import scala.util.parsing.combinator._

class CalculatorParser extends JavaTokenParsers {

    def num: Parser[Double] = floatingPointNumber ^^ (_.toDouble)

    def const: Parser[Double] = ( "pi" | "e" ) ^^ {
      case "pi" => math.Pi.toDouble
      case "e" => math.E.toDouble
    }

    def operator: Parser[String] = ("*" | "/" | "+" | "-" | "^"); 

    def term: Parser[Double] = num | const | function | "(" ~> expression <~ ")" ^^ (_.toDouble)

    def function: Parser[Double] = ("sin" | "cos" | "tan") ~ ("(" ~> expression <~ ")") ^^ { case opp ~ v => {
        opp match {
          case "sin" => math.sin(v)
          case "cos" => math.cos(v)
          case "tan" => math.tan(v)
        }
      }
    }
      
    def expression: Parser[Double] = (term ~ rep(operator ~ term)) ^^ {
      case (b ~ list) => {
        val precedence = "^"::"/"::"*"::"+"::"-"::List.empty;
        val l = {("+",b) :: (list.map( x=>(x._1,x._2) )) };
        precedence.foldLeft[List[(String, Double)]](l)((li, expr) => {
          li.foldLeft[List[(String, Double)]](List.empty)( (acc, x) => {
            if( x._1 == expr ) {
              if( acc.isEmpty ) {
                acc :+ x
              } else {
                acc.dropRight(1) :+ ( acc.last._1, operatorFn(x._1)(acc.last._2, x._2)) 
              }
            } else {
              acc :+ x
          }
          })
        } ).head._2
      }
    }
    def operatorFn( op:String ) : (Double, Double) => Double = {
      op match {
        case "+" => (x, y) => x + y
        case "-" => (x, y) => x - y
        case "*" => (x, y) => x * y
        case "/" => (x, y) => x / y
        case "^" => (x, y) => math.pow(x, y)
      }
    }
}
