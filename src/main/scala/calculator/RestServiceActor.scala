package calculator

import akka.actor.Actor
import akka.event.slf4j.SLF4JLogging
import java.text.{ParseException, SimpleDateFormat}
import java.util.Date
import net.liftweb.json.Serialization._
import net.liftweb.json.{DateFormat, Formats}
import scala.Some
import spray.http._
import spray.httpx.unmarshalling._
import spray.routing._

/**
 * REST Service actor.
 */
class RestServiceActor extends Actor with RestService {

  implicit def actorRefFactory = context

  def receive = runRoute(rest)
}

/**
 * REST Service
 */
trait RestService extends HttpService with SLF4JLogging {
  val rest = respondWithMediaType(MediaTypes.`application/json`) {
    path("calc") {
        get {
          parameters('q) {
            query: String => {
              ctx: RequestContext =>
                handleRequest(ctx) {
                  log.debug("running query: %s".format(query));
                  val parser = new CalculatorParser();
                    parser.parseAll(parser.expression, query) match {
                      case parser.Success(res, _) => {
                        Right( "%10.7f".format(res.toFloat)
                          .replaceFirst("\\.?0*$",""))
                      }
                      case x=> {
                        log.debug("error: %s".format(x));
                        Right("ERR")
                      }
                    }
                }
            }
          }
        }
    }
  }


  implicit val formats = net.liftweb.json.DefaultFormats;
  /**
   * Handles an incoming request and create valid response for it.
   *
   * @param ctx         request context
   * @param successCode HTTP Status code for success
   * @param action      action to perform
   */
  protected def handleRequest(ctx: RequestContext, successCode: StatusCode = 200)(action: => Either[String, _]) {
    action match {
      case Right(result: String) =>
        ctx.complete(successCode, net.liftweb.json.Serialization.write(Map("result"-> result)))
      case _ =>
        ctx.complete(StatusCodes.InternalServerError)
    }
  }
}
