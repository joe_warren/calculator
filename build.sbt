name := "rest"

version := "1.0"

scalaVersion := "2.11.4"

libraryDependencies ++= Seq(
    "io.spray" %% "spray-can" % "1.3.3",
    "io.spray" %% "spray-http" % "1.3.3",
    "io.spray" %% "spray-routing" % "1.3.3",
    "com.typesafe.akka" %% "akka-actor" % "2.3.9",
    "com.typesafe.akka" %% "akka-slf4j" % "2.3.9",
    "net.liftweb" %% "lift-json" % "2.6.1",
    "ch.qos.logback" % "logback-classic" % "1.0.13",
    "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.4"
)

resolvers ++= Seq(
    "Spray repository" at "http://repo.spray.io",
    "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"
)
